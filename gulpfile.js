// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const replace = require('gulp-replace');
const sass = require('gulp-sass')(require('sass'));

// File paths
const files = { 

    scssPath: [ 

        'scss/*.scss'

    ],

    npmPathsJS: [

        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'

    ],

    jsPaths: [

        'js/framework.js'

	],
	
}

function move_fonts(){
	
    src( [ 'src/fontawesome-subset/webfonts/*.{ttf,woff,woff2,eot,eof,svg}' ] )
        .pipe(dest('webfonts')
    );	

}

function create_framework(){
    return src( files.npmPathsJS )
        .pipe(concat('framework.js'))
        .pipe(uglify())
        .pipe(dest('js')
    );
}

// Sass task: compiles the style.scss file into style.css
function scssTask(){    
    return src(files.scssPath)
        .pipe(sass({outputStyle: 'compressed'})) // compile SCSS to CSS
        .pipe(dest('./')); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src( files.jsPaths )
        .pipe(concat('core.min.js'))
        .pipe(uglify())
        .pipe(dest('js')
    );
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){

    watch(files.jsPaths,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( jsTask )
        )
    );
    watch(['src/fontawesome-subset/css/all.css'],
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( move_fonts )
        )
    );      
    watch(files.scssPath,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( scssTask )
        )
    );
  	
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    create_framework,
    move_fonts,
    scssTask,
    jsTask,
    watchTask,
);