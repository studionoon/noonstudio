/*!
 * Font Awesome Pro 5.15.3 by @fontawesome - https://fontawesome.com
 * License - https://fontawesome.com/license (Commercial License)
 */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (factory((global['pro-regular-svg-icons'] = {})));
}(this, (function (exports) { 'use strict';

  var prefix = "far";
  var faBars = {
    prefix: 'far',
    iconName: 'bars',
    icon: [448, 512, [], "f0c9", "M436 124H12c-6.627 0-12-5.373-12-12V80c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12z"]
  };
  var _iconsCache = {
    faBars: faBars
  };

  exports.far = _iconsCache;
  exports.prefix = prefix;
  exports.faBars = faBars;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
