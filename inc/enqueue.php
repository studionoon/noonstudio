<?php
/**
 * Enqueue Script
 **
 * @package TMI_Analytics
 */


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	global $wp_query;
	

	// Styles
    wp_enqueue_style( 'bootstrap', get_stylesheet_uri(), array(), _STUDIO_NOON_VERSION );

	// Scripts
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/core.min.js', array( 'jquery' ), _STUDIO_NOON_VERSION, true );

}
